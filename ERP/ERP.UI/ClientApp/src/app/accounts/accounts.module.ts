import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountsRoutingModule } from './accounts-routing.module';
import { GeneralLedgerComponent } from './general-ledger/general-ledger.component';
import { AddEditAccountComponent } from './general-ledger/add-edit-account/add-edit-account.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import{HttpClientModule, HTTP_INTERCEPTORS}from '@angular/common/http';
import { AccountListingComponent } from './general-ledger/account-listing/account-listing.component';
import { MaterialModule } from '../material/material.module';
import { MatConfirmDialogComponent } from '../mat-confirm-dialog/mat-confirm-dialog.component';
import { GerenalJounalComponent } from './gerenal-jounal/gerenal-jounal.component';
import { PurchaseJounalComponent } from './purchase-jounal/purchase-jounal.component';
import { DataTablesModule } from 'angular-datatables';
@NgModule({
  declarations: [GeneralLedgerComponent, AddEditAccountComponent, AccountListingComponent,MatConfirmDialogComponent, GerenalJounalComponent, PurchaseJounalComponent],
  imports: [
    CommonModule,
    AccountsRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    MaterialModule,
    DataTablesModule
  ],
  exports:[AddEditAccountComponent],
  entryComponents:[AddEditAccountComponent,MatConfirmDialogComponent]
})
export class AccountsModule { }
