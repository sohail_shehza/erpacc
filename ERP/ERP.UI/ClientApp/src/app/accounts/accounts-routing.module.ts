import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from '../layout/layout.component';
import { AccountListingComponent } from './general-ledger/account-listing/account-listing.component';


const routes: Routes = [
  // { path: 'dashboard', component: AccountListingComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountsRoutingModule { }
