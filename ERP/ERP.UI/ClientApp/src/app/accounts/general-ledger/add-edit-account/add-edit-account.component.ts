import { Component, OnInit, HostListener } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { EnumHelper } from 'src/app/Helpers/enum-helper';
import { AccountType } from 'src/app/Enums/account-type.enum';
import { AccountService } from 'src/app/Services/Account/account.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-edit-account',
  templateUrl: './add-edit-account.component.html',
  styleUrls: ['./add-edit-account.component.css'],
  providers:[EnumHelper]
})
export class AddEditAccountComponent implements OnInit {
 
  Accounts;
  AccType;
  // constructor(public service:AccountService,
  //   public dialogRef: MatDialogRef<AddEditAccountComponent>,
  //   public accountType:EnumHelper) { }
  constructor(public service:AccountService,public accountType:EnumHelper)
   { }
  ngOnInit() {

    this.service.GetListAccount().subscribe(
      (res:any)=>{
        this.Accounts=res;
      }
    )
    this.AccType=this.accountType.GetEnumLookUp(AccountType);
  }
  
  
  onSubmit(){
    this.service.CreateAccount().subscribe(
      (res:any)=>{
        debugger
      },
      err=>{console.log(err)
        debugger
        if(err.status==200)
        {
         // this.onClose();
        }
      }
    )
  }
  
  // onClose() {
  //   this.dialogRef.close();
  // }
  
}

