import { Component, ViewChild,OnInit, HostListener } from '@angular/core';
import {MatTableDataSource,MatSort,MatPaginator,MatDialogRef, MatDialogConfig, MatDialog} from '@angular/material'
import { from } from 'rxjs';
import { AddEditAccountComponent } from '../add-edit-account/add-edit-account.component';
import { DialogService } from 'src/app/shared/dialog.service';
import { AccountService } from 'src/app/Services/Account/account.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
declare var $;
@Component({
  selector: 'app-account-listing',
  templateUrl: './account-listing.component.html',
  styleUrls: ['./account-listing.component.css']
})
export class AccountListingComponent implements OnInit {
// @HostListener('window:keydown.space') spaceEvent()
@HostListener('window:keyup.alt.n') createAccount()
{
 this.onCreate();
}
@HostListener('window:keyup.esc') closeModal()
{
 this.onClose();
}

Assests;
Capital;
Liabilities;
Expenses;
Revenue;
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['Refrence', 'Code', 'Title','isActive','actions'];
  @ViewChild(MatSort,{static:true}) sort: MatSort;
  @ViewChild(MatPaginator,{static:true}) paginator: MatPaginator;
  searchKey: string;
  closeResult: string;
  Accounts;
  readonly BaseURI="http://localhost:52310/api/values";
  constructor(private accService:AccountService,
    private addEditService:AccountService,
    private dialog: MatDialog,private dialogService: DialogService,private modalService: NgbModal) { 
       this.Listing();
    }
  ngOnInit() { 
    this.accountType();
  }

  accountType()
  {

     this.Assests = [
      {"id":1,"name":"Cash","quantity":56840},
      {"id":2,"name":"Stock","quantity":9358},
      {"id":3,"name":"Test Party Account","quantity":90316},
      {"id":4,"name":"Test 3 Account","quantity":5899}
  ];
  this.Capital = [
    {"id":1,"name":"Capital","quantity":56840},
    {"id":2,"name":"Capital1","quantity":9358}
];
this.Liabilities=[
  {"id":1,"name":"Awais Account","quantity":56840},
  {"id":2,"name":"Zahid Account","quantity":9358},
  {"id":3,"name":"Hamza Ali Account","quantity":90316},
  {"id":4,"name":"hamza Account","quantity":5899},
  {"id":2,"name":"Waleed Account","quantity":9358},
  {"id":3,"name":"Duplication Account","quantity":90316},
  {"id":4,"name":"Zahid Account","quantity":5899}
];
this.Expenses = [
  {"id":1,"name":"Purchase","quantity":56840},
  {"id":2,"name":"Lunch","quantity":9358},
  {"id":1,"name":"Purchase","quantity":56840},
  {"id":2,"name":"Lunch","quantity":9358},
  {"id":1,"name":"Purchase","quantity":56840},
  {"id":2,"name":"Lunch","quantity":9358},
  {"id":1,"name":"Purchase","quantity":56840},
  {"id":2,"name":"Lunch","quantity":9358},
  {"id":1,"name":"Purchase","quantity":56840},
  {"id":2,"name":"Lunch","quantity":9358}
];
this.Revenue = [
  {"id":1,"name":"Sales","quantity":56840},
  {"id":2,"name":"Sales property","quantity":9358}
];
  }
  openFormModal() {
    this.accService.resetForm();
    const modalRef = this.modalService.open(AddEditAccountComponent);
  
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
  }
  Listing()
  {
    this.accService.getAccountListing().subscribe(
      (res:any) => {
        this.listData = new MatTableDataSource(res);
        this.listData.sort = this.sort;
        this.listData.paginator = this.paginator;
        this.listData.filterPredicate = (data, filter) => {
          return this.displayedColumns.some(ele => {
            debugger
            return ele != 'actions' && data[ele].toLocaleLowerCase().indexOf(filter) != -1;
          });
        };
      },
      err=>{console.log(err)
        
      });
  }
  onEdit(row){
     this.addEditService.populateForm(row);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    //dialogConfig.position=
    dialogConfig.width = "50%";
   // dialogConfig.height = "65%";
    this.dialog.open(AddEditAccountComponent,dialogConfig);
  }
 
  onCreate() {
    this.addEditService.resetForm();
     const dialogConfig = new MatDialogConfig();
     dialogConfig.disableClose = true;
     dialogConfig.autoFocus = true;
    //dialogConfig.height = "65%";
    dialogConfig.width = "50%";
     this.dialog.open(AddEditAccountComponent,dialogConfig);
   }
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
 

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLocaleLowerCase();
  }
  
  onDelete(rec){
    this.dialogService.openConfirmDialog('Are you sure to delete this record ?')
    .afterClosed().subscribe(res =>{
      if(res){
        console.log(res)
        this.accService.DeleteAccount(rec.id).subscribe(
          (res:any)=>{
            debugger
          alert(res)
          }
        )
        //this.notificationService.warn('! Deleted successfully');
      }
    });
  }
  onClose() {
    this.dialog.closeAll();
  }

}

