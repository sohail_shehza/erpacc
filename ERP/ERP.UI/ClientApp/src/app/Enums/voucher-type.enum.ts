export enum VoucherType {
    DebitVoucher=1,
    CashVoucher=2,
    BankVoucher=3,
    TransferVoucher=4,
    JournalVoucher=5,
    SupportVoucher=6
}
