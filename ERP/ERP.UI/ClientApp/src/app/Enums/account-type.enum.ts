export enum AccountType {
        Assets = 1,
        Revenue = 2,
        Liabilities = 3,
        Equity = 4,
        Expenses = 5
}
