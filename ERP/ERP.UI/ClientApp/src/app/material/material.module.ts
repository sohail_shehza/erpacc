import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as Material from "@angular/material";
import { MatInputModule, MatFormFieldModule, MatButtonModule, MatCheckboxModule, MatSelectModule, MatDatepickerModule, MatNativeDateModule, MatGridListModule, MatTableModule, MatPaginatorModule, MatIconModule, MatDialogModule, MatToolbarModule, MatTabsModule } from '@angular/material';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    Material.MatGridListModule, // for grid column
    Material.MatFormFieldModule, //form
    Material.MatInputModule, //input
    Material.MatRadioModule, //mat-radio-group - radio button
    Material.MatDatepickerModule, //matDatepicker - datepicker
    Material.MatNativeDateModule, //required for datepicker
    Material.MatSelectModule, //mat-select - dropdown
    Material.MatCheckboxModule, //mat-checkbox - checkbox
    Material.MatButtonModule, // for button
    Material.MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatGridListModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule, 
    MatDialogModule,
    MatToolbarModule,
    MatTabsModule
  ],
  exports:[
    Material.MatGridListModule,
    Material.MatFormFieldModule,
    Material.MatInputModule,
    Material.MatRadioModule,
    Material.MatDatepickerModule,
    Material.MatNativeDateModule,
    Material.MatSelectModule,
    Material.MatCheckboxModule,
    Material.MatButtonModule,
    Material.MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatGridListModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule, 
    MatDialogModule,
    MatToolbarModule,
    MatTabsModule
  ]
})
export class MaterialModule {

  
 }
