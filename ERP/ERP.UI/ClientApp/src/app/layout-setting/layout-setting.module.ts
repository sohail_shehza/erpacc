import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutSettingRoutingModule } from './layout-setting-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LayoutSettingRoutingModule
  ]
})
export class LayoutSettingModule { }
