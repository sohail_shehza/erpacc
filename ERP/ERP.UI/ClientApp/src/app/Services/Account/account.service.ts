import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  readonly BaseURI="http://localhost:52310/api";
  constructor(private fb:FormBuilder,private http:HttpClient) { }
  formModel=this.fb.group({
    RefId:[''],
    AccountCode:[''],
    Title:['',Validators.required],
    ParentId:[''],
    IsActive:[''],
    Id:[''],
    pAccount:[''],
    Type:['',[Validators.required]],
    Balance:['',Validators.required],
    IsCredit:['']
    })

  getAccountListing(){
    return this.http.get(this.BaseURI+'/accounts/AccoutListing');

  }
  DeleteAccount(id){

    
    return this.http.get(this.BaseURI+"/accounts/DeleteAccount?id="+id);
  }
  populateForm(account) {
    debugger
    this.formModel.setValue({RefId:account.refId,
      AccountCode:account.code,Title:account.title,
      pAccount:account.title,IsActive:account.isActive,
      Id:account.id,
      ParentId:account.id,
      Type:account.type>0?account.type:0,
      Balance:null,
      IsCredit:true
    })
  }
  resetForm() {
    this.formModel.setValue({RefId:0,
      Title:"",
      AccountCode:"",
      pAccount:"",
      IsActive:true,
      Id:0,
      ParentId:0,
      Type:0,
      Balance:null,
      IsCredit:true
    })
  }
  CreateAccount(){
    var createEdit='';
    var body={
     // RefId:this.formModel.value.RefId,
      RefId:this.formModel.value.ParentId,
      Code:this.formModel.value.AccountCode,
      Title:this.formModel.value.Title,
      IsActive:true,
       ParentId:this.formModel.value.ParentId,
       Id:this.formModel.value.Id>0?this.formModel.value.Id:0,
       Type:this.formModel.value.Type?this.formModel.value.Type:0,
       Balance:this.formModel.value.Balance,
       IsCredit:true
    }
    if(this.formModel.value.Id>0){
      createEdit='Edit';
    }else{
      createEdit='Create';
    }
    return this.http.post(this.BaseURI+"/accounts/"+createEdit,body);
  
  }
  GetListAccount(){
    var get= this.http.get(this.BaseURI+"/accounts/Listing");
    return get;
  }
  PurchaseJournal()
  {
    var get= this.http.get(this.BaseURI+"/accounts/Listing");
    return get;
  }
}
