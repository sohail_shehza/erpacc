import { Injectable } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import{HttpClient, HttpHeaders}from '@angular/common/http'
import { from } from 'rxjs';
import { element } from 'protractor';
@Injectable({
  providedIn: 'root'
})
export class UserService {
readonly BaseURI="http://localhost:51346/api";
  constructor(private fb:FormBuilder,private http:HttpClient) { }
  formModel=this.fb.group({
    UserName:['',Validators.required],
    Email:['',Validators.email],
    FullName:[''],
    Passwords:this.fb.group({
      Password:['',[Validators.required,Validators.minLength(4)]],
    ConfirmPassword:['',Validators.required]
    },{Validators:this.comparePassword})
   
   
  });

  comparePassword(fb:FormGroup){
    let confirmPswrdCtrl=fb.get('ConfirmPassword');
    //passwordMissmatch
    //confirmPswrdCtrl.errros={confirmPswrdCtrl:true}
    if(confirmPswrdCtrl.errors==null || 'passwordMismatch' in confirmPswrdCtrl.errors){
      if(fb.get('Password').value!=confirmPswrdCtrl.value){
        confirmPswrdCtrl.setErrors({passwordMismatch:true})
      }
      else
      confirmPswrdCtrl.setErrors(null);
    }
  }    
  register(){
    var body={
      UserName:this.formModel.value.UserName,
      Email:this.formModel.value.Email,
      FullName:this.formModel.value.FullName,
      Password:this.formModel.value.Passwords.Password,
    }
    return this.http.post(this.BaseURI+"/ApplicationUser/register",body);
  }
  login(formData){
    debugger
    return this.http.post(this.BaseURI+"/ApplicationUser/Login",formData);
  }
  getUserProfile(){
    debugger
// var tokenHeader=new HttpHeaders({'Authorization':'Bearer ' +localStorage.getItem('token')})
// var respone= this.http.get(this.BaseURI+'/UserProfile',{headers:tokenHeader});
return this.http.get(this.BaseURI+'/UserProfile');
  }
  roleMatch(allowedRoles):boolean{
var isMatch=false;
var payLoad=JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]))
var userRoles=payLoad.role;
allowedRoles.forEach(element=>{
  if(userRoles==element){
    isMatch=true;
    return false;
  }
  
});
return isMatch;
  }
}
