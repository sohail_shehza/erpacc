import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountListingComponent } from './accounts/general-ledger/account-listing/account-listing.component';
import { LoginComponent } from './user/login/login.component';
import { LayoutComponent } from './layout/layout.component';
import { PurchaseJounalComponent } from './accounts/purchase-jounal/purchase-jounal.component';

const routes: Routes = [
  
{path:'login',component:LoginComponent,

},
{
  path:'',component:LayoutComponent,
  children: [
    { path: '', component: AccountListingComponent },
    
    { path: 'GerenalJounal', component:PurchaseJounalComponent },
  ]
},
// {
//   path:'',component:LayoutComponent,
//   loadChildren: () => import('./accounts/general-ledger/account-listing/account-listing.component').then(m => m.AccountListingComponent)
// },
{ path: '**', redirectTo: 'login' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
