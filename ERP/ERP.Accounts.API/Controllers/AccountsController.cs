﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.Accounts.BLL.Account;
using ERP.Accounts.Entities;
using ERP.Repo;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Accounts.API.Controllers
{
    //[Route("api/accounts/[controller]")]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        IUnitOfWork<Account> _accountUOW;
        public AccountsController(IUnitOfWork<Account> accountUOW)
        {
            _accountUOW = accountUOW;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }
     
        public IEnumerable<string> List()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        //[HttpDelete("{id}")]
        //[Route("DeleteAccount")]
        //public void Delete(int id)
        //{
        //}

        [HttpGet]
        [Route("DeleteAccount")]
        public bool Delete(long id)
        {
            _accountUOW.Repo.Delete(id);
            _accountUOW.Save();
            return true;
        }

        [HttpPost]
        [Route("Create")]
        public IActionResult CreateAccount(Account account)
        {
            if(account.Balance!=null && account.Balance>0)
            {
                AccountDetail details = new AccountDetail();
                details.CreatedOn = DateTime.UtcNow;
                details.IsActive = true;
                details.Description = "Account opening balance";
                if(account.IsDebit)
                {
                    details.Debit = account.Balance;
                }
                else
                {
                    details.Credit = account.Balance;
                }
                Transaction transaction = new Transaction();
                transaction.Date = DateTime.UtcNow;
                transaction.Description = details.Description;
                transaction.IsActive = true;
                details.Transactions = transaction;
                account.Details.Add(details);
            }
            _accountUOW.Repo.Insert(account);
            _accountUOW.Save();
            return Ok(true);
        }
        [HttpPost]
        [Route("Edit")]
        public IActionResult Edit(Account account)
        {
           
            _accountUOW.Repo.Update(account);
            _accountUOW.Save();
            return Ok("Account updated successfully");
        }

        [HttpGet]
        [Route("AccoutListing")]
        public IActionResult AccListing(string draw, int start, int length, string search)
        {
            var data = _accountUOW.Repo.GetAll().Where(x=>x.IsActive);

            var account = data.Select(x => new Account { Id=x.Id, IsActive = x.IsActive, Code= x.Code, RefId = x.RefId, Title = x.Title,Type=x.Type });

            return Ok(account);
        }
        [HttpGet]
        [Route("Listing")]
        public IActionResult AccountDDL()
        {
            List<Account> accounts = new List<Account>();
            accounts = _accountUOW.Repo.GetAll().Select(x=>new Account {Id= x.Id,Title= x.Title}).ToList();
            return Ok(accounts);
        }
        
    }
}