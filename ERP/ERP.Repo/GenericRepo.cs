﻿using System;
using System.Collections.Generic;
using System.Text;
using ERP.Common;
using Microsoft.EntityFrameworkCore;

namespace ERP.Repo
{
    public class GenericRepo<T> : IGenericRepo<T> where T: BaseEntity
    {
        private DbContext _context;
        private DbSet<T> _entity = null;
        public GenericRepo(DbContext context)
        {
            _context = context;
            _entity = _context.Set<T>();
        }
        public void Delete(object id)
        {
           var en = _entity.Find(id);
            en.IsActive = false;
        }

        public IEnumerable<T> GetAll()
        {
            return _entity;
        }

        public T GetById(object id)
        {
            return _entity.Find(id);
        }

        public void Insert(T obj)
        {
            _entity.Add(obj);
        }

        public void Update(T obj)
        {
            _entity.Attach(obj).State = EntityState.Modified;
        }
    }
}
