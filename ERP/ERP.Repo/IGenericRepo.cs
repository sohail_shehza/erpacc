﻿using ERP.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ERP.Repo
{
    public interface IGenericRepo<T> where T:BaseEntity
    {
        IEnumerable<T> GetAll();
        T GetById(object id);
        void Insert(T obj);
        void Update(T obj);
        void Delete(object id);
    }
}
