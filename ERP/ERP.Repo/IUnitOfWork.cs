﻿using ERP.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ERP.Repo
{
    public interface IUnitOfWork<T> where T:BaseEntity
    {
        IGenericRepo<T> Repo { get; }
        void Save();
    }
}
