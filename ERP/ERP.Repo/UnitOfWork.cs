﻿using ERP.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ERP.Repo
{
    public class UnitOfWork<T> : IUnitOfWork<T> where T : BaseEntity
    {
        private IGenericRepo<T> _repo;
        private DbContext _context;
        public UnitOfWork(DbContext context)
        {
            _repo = new GenericRepo<T>(context);
            _context = context;
        }

        IGenericRepo<T> IUnitOfWork<T>.Repo { get => _repo ?? new GenericRepo<T>(_context); }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
