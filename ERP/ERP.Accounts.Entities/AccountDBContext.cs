﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ERP.Accounts.Entities
{
    public class AccountDBContext:DbContext
    {
        public AccountDBContext(DbContextOptions<AccountDBContext> options) : base(options)
        { }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("server=.;database=myDb;trusted_connection=true;");
        //}

        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountDetail> AccountDetails { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

    }
}
