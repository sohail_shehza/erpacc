﻿using ERP.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ERP.Accounts.Entities
{
    public class AccountDetail:BaseEntity
    {
        public long AccountId { get; set; }
        public decimal? Debit { get; set; }
        public decimal? Credit { get; set; }
        public string Description { get; set; }
        public long? TransactionId { get; set; }

        [ForeignKey("AccountId")]
        public virtual Account Accounts { get; set; }
        [ForeignKey("TransactionId")]
        public virtual Transaction Transactions { get; set; }


    }
}
