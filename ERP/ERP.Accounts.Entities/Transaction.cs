﻿using ERP.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ERP.Accounts.Entities
{
    public class Transaction : BaseEntity
    {
        public string Description { get; set; }
        public DateTime Date { get; set; }

        public virtual ICollection<AccountDetail> Details { get; set; }
    }
}
