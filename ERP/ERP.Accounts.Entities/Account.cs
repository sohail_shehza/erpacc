﻿using ERP.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ERP.Accounts.Entities
{
    public class Account:BaseEntity
    {
        public Account()
        {
            //Accounts = new HashSet<Account>();
            Details = new HashSet<AccountDetail>();
        }
        public long? RefId { get; set; }
        public string Code { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public Int16 Type { get; set; }
        public long? ParentId { get; set; }
        public bool IsParty { get; set; }

        [ForeignKey("ParentId")]
        public virtual ICollection<Account> Accounts { get; set; }
        public virtual ICollection<AccountDetail> Details { get; set; }


        [NotMapped]
        public decimal? Balance { get; set; }
        [NotMapped]
        public bool IsDebit { get; set; }
    }
    
}
