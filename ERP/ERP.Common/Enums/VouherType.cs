﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ERP.Common.Enums
{
    public enum VouherType
    {
        DebitVoucher = 1,
        CashVoucher = 2,
        BankVoucher = 3,
        TransferVoucher = 4,
        JournalVoucher = 5,
        SupportVoucher = 6
    }
}
