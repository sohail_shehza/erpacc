﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ERP.Common.Enums
{
    public enum EnumType
    {

        Assets = 1,
        Revenue = 2,
        Liabilities = 3,
        Equity = 4,
        Expenses = 5
    }
}
